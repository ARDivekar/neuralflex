import math
import random
import MNIST_handler
import gc
from copy import deepcopy

def matrix_binarizer(eg):
    eg = deepcopy(eg)
    for i in range(0, len(eg)):
        for j in range(0, len(eg[i])):
            if eg[i][j] > 0:
                eg[i][j] = 1
            else:
                eg[i][j] = 0
    return eg

def array_binarizer(eg):
    eg = deepcopy(eg)
    for i in range(0, len(eg)):
        if eg[i] > 0:
            eg[i] = 1
        else: eg[i] = -1
    return eg


def replace_zeros_with_spaces(eg):
    eg = deepcopy(eg)
    for i in range(0, len(eg)):
        for j in range(0, len(eg[i])):
            if eg[i][j] == 0:
                eg[i][j] = ' '
    return eg

def array_replace_zeros_with_spaces(eg):
    eg = deepcopy(eg)
    for i in range(0, len(eg)):
        if eg[i] == 0:
            eg[i] = ' '
    return eg


def see(eg_input, eg_output=None):
    eg = []
    ## At present, each input example in MNIST is in an array of 784 elements, representing a 28x28 image.
    ## All values are the gray-level values of the image, between 0 and 255.
    ## We want to visualize the image of a digit, so we temporarily convert the input example array into a 28x28 array-of-arrays, and then iterate through it and print all values, with correct spacing.

    ## Convert:    784-integer vector `eg_input`    ====>    28x28 matrix `eg`
    side_size = int(math.sqrt(len(eg_input)))
    for i in range(0, side_size):
        eg.append(eg_input[side_size*i:side_size*(i+1)])

    print("\n\n"+"-"*side_size+"\n"+" "*2+"Actual Data (visualized):\n"+"-"*side_size)
    ## Idea from http://stackoverflow.com/a/15187932/4900327:
    for x in eg:
        outs = "%4s"*len(x)
        print(outs%tuple(x))
    if eg_output is not None:
        print("\n^^ The output value is: %s"%(eg_output))


def see_train(n=5):
    '''Visualize `n` randomly sampled datapoints from the MNIST training set.'''
    train_inputs, train_outputs, test_inputs, test_outputs = MNIST_handler.NUMPY.load_all()
    ## Now, we generate `n` random samples from the training set to see what we're dealing with.
    for i in random.sample(range(0, len(train_inputs)), n):    ## Source for random.sample(...) => http://stackoverflow.com/a/22842411/4900327
        see(train_inputs[i], train_outputs[i])
        print("\n")
    gc.collect()

def see_test(n=5):
    '''Visualize `n` randomly sampled datapoints from the MNIST test set.'''
    train_inputs, train_outputs, test_inputs, test_outputs = MNIST_handler.NUMPY.load_all()
    ## Now, we generate `n` random samples from the test set to see what we're dealing with.
    for i in random.sample(range(0, len(test_inputs)), n):    ## Source for random.sample(...) => http://stackoverflow.com/a/22842411/4900327
        see(test_inputs[i], test_outputs[i])
        print("\n")
    gc.collect()
