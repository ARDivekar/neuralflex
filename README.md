Open `Network.py` to see the code. There are at present two tests: one on IRIS and one on MNIST for Digit Recognition.
 - IRIS is completely present in `iris_dataset.py`
 - MNIST is a standard Digit Recognition dataset with 60,000 training and 10,000 test examples of 28x28 Black-and-White images. It's commonly used for OCR models. See http://yann.lecun.com/exdb/mnist/ for full details (note that I claim no rights to the MNIST dataset whatsoever).
 To work with MNIST, run `$ cd MNIST_dataset_for_Digit_Recognition/` and follow its `INSTRUCTIONS.txt` to get the dataset in various formats. To test `Network.py` with MNIST, you must create the NUMPY format (see `INSTRUCTIONS.txt`).

The `References/` folder has some (very readable) documents to better understand the Backpropagation algorithm. I'm writing my own in `Backpropagation writeup/`, with an explicit and easy-to-understand notation that to help programmers write Neural Networks.
