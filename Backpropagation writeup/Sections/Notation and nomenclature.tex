
\section{Notation and nomenclature:}

One of the biggest problem with understanding the Backpropagation algorithm is notation. Since Neural Networks are large, complex beasts with several connections, inputs and outputs, and neurons arranged in different layers, most explanations only deal with one part of the system at a time so as to not overwhelm first-time readers. However, in doing so, they often use simplistic notation that makes sense in the context under consideration, but does not allow the reader to understand how it may be applicable to other, analogous contexts. It's also often difficult to ascertain the dimensionality of variables when using this simplistic notation.

For this reason, every value in this document is represented as explicitly as possible: be it a connection weight, neuron input or output. If values must be aggregated into vectors or matrices, then the notation simply removes the relevant indexes (\textit{i}, \textit{j}, etc.) for that aggregation; this means that the vector contains values for all possible indexes that variable.

We describe the notation used by this document in the following subsections.

\subsection{General nomenclature:}
\begin{description}[font=$\bullet$ \scshape\bfseries]
	\item \textit{Neural Network:} a network of neurons arranged into $L$ hidden layers, $l_1, l_2,..., l_L$, where each hidden layer has $N_1, N_2,..., N_L$ neurons and 1 bias unit; except the final \textit{output layer} $l_o$, which only has output neurons. $N_i \geq 1$.
	
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=0pt]
		\item A Neural Network may be used for \textit{classification} or \textit{regression}. Here, we consider the classification case: the output layer $l_o$ has $K$ neurons, one for each class. Only a single neuron is allowed to predict 1, and all the others predict 0. 
		
		\item Every neuron of a layer $l_c$ is connected to every neuron of it's next layer $l_{(c+1)}$. Every such connection is weighted. Neurons in layer $l_c$ are said to \textit{output} computed values which are \textit{input} to neurons in layer $l_{(c+1)}$. 
		
		\item Each hidden layer has a vector or weights $w$ associated with each of its neurons. The weights are used to predict outcomes for unknown examples. Training these weights as optimally as possible is the purpose of the \textit{Backpropagation algorithm}.
		
		
	\end{description}

	\item \textit{Dataset}: we use a training dataset to find the optimal weights. The objective of the Backpropagation algorithm is to reduce the \textit{training error}, \textit{E} which is produced over the dataset.
	
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=2pt]
	
		\item The dataset is assumed to be an $(M \times F)$ matrix, where \textit{M} is the number of \textit{training examples} and \textit{F} is the number of \textit{features}. 
	
		\item Our Neural Networks here are supervised learning models: we assume each \textit{training example} $X^i$ has an associated \textit{teacher value}, $Y^i$ which is used to tell the NN what the the "correct" output is for that example. For the classification case, the set of values $Y^i$ takes is finite. We assume the number of such \textit{classes}, is $K$. 
		Thus $Y^i \in \{1, 2, 3,\hdots, k, \hdots, K\}$
		
		($Y^i$ is often called the "ouput" value, but we refrain from using that name, so as to prevent confusion with the outputs of each neuron).
		
		\item Training $X^i$ is a row of values, one per feature. We index each value as: $X^i_f$.
	
	
		\item Here is an example classification dataset which we may use to predict the city in which a house is located. Each row is a new training example $X^i$, representing a different apartment. The columns are features, labeled with their real-world meanings. 

		\tabulinesep=6pt	% Source: tex.stackexchange.com/a/207146/110560
		\begin{longtabu} to \textwidth {| l  c  c  c  c | c c |} \hline
			& monthly 	& number of	& number of & Internet 	& & City \vspace{-10pt}\\
			& rent 		& bedrooms 	& bathrooms & connection& &  \\ \hline
			$X^1:$	& 3350.0	& 2 	& 1 & Yes 	& $Y^1:$ & San Francisco \\ \hline
			$X^2:$	& 2500.0	& 1 	& 1 & No 	& $Y^2:$ & San Francisco \\ \hline
			$X^3:$	& 2200.0	& 1 	& 1 & Yes 	& $Y^3:$ & New York \\ \hline
			$X^4:$	& 1190.0	& 1 	& 1 & On\_demand 	& $Y^4:$ & Austin \\ \hline
			$X^5:$	& 1380.0	& 2 	& 2 & No 	& $Y^5:$ & Austin \\ \hline
			$X^6:$	& 4200.0	& 3 	& 2 & Yes 	& $Y^6:$ & San Francisco \\ \hline
			$X^7:$	& 3700.0	& 2 	& 1 & Yes 	& $Y^7:$ & New York \\ \hline
			\caption{ % See \captionsetup in the main file for [longtable] capton settings.
				City housing dataset. \\
				M = 7 training examples: $\{(X^1, Y^1),\hdots, (X^7, Y^7)\}$, \\
				F = 4 = $ | \{Rent, \#Bedrooms, \#Bathrooms, Internet\} | $ \\
				K = 3 = $ | \{San Francisco, New York, Austin\} | $
			}
		\end{longtabu}
		
		\vspace{-12pt}
		
		In general, every value in the dataset must be a number. For categorical features, we can encode each category as a number. E.g. for \textit{Internet connection}, we can encode \{No $\Rightarrow$ 0, Yes $\Rightarrow$ 1, On\_demand $\Rightarrow$ 2\}. Several Machine Learning libraries have an inbuilt encoder for this purpose. 
		
		\item The above is a generic Machine Learning dataset representation. For Neural Networks in the classification scenario, we need to alter the teacher values; we convert each teacher value $Y^i$ into a binary vector:
		\begin{equation*}
		Y^i = \begin{bmatrix} Y^i_1, Y^i_2, \hdots, Y^i_K \end{bmatrix}
		\end{equation*}
		
		Each index $k$ corresponds to a different class. Each of these $Y^i_k$ values is binary (0 or 1). All of them are 0, except for the index of the teacher value for that training example. 
		
		For the table above, the teacher column under this format becomes:
		
		\tabulinesep=6pt	% Source: tex.stackexchange.com/a/207146/110560
		\begin{longtabu} to \textwidth {| c c c c |} \hline
					&  				& City	= Y	&  			\\ \hline
				 	& San Francisco	& New York 	& Austin 	\vspace{-6pt} \\ 
				 	& $Y^m_1$:		& $Y^m_2$:	& $Y^m_k$	\\ \hline   
			$Y^1:$ 	& [ 1 			& 0 		& 0 ] 		\\ \hline
			$Y^2:$ 	& [ 1 			& 0 		& 0 ]		\\ \hline
			$Y^3:$ 	& [ 0 			& 1 		& 0 ]		\\ \hline
			$Y^4:$ 	& [ 0 			& 0 		& 1 ]		\\ \hline
			$Y^5:$ 	& [ 0 			& 0 		& 1 ]		\\ \hline
			$Y^6:$ 	& [ 1 			& 0 		& 0 ]		\\ \hline
			$Y^7:$ 	& [ 0 			& 1 		& 0 ]		\\ \hline
			\caption{City housing dataset with teacher values as binary vectors.}
		\end{longtabu}
		 
		 
		
		\item Our goal is to train a NN model which, given a training example from the dataset, is able to reproduce it's correct teacher value. 
		
		[\textit{Side Note:} the objective of Cross-Validation is to find values for the Neural Network \textit{parameters} which give the smallest Cross-Validation error].
		
		
	
	
	\end{description}
	
	\item \textit{Hidden layer:} \\A layer of neurons, each of which takes inputs and bias from every neuron in the previous layer and calculates an output $z$, a real-valued scalar. Each neurons in a hidden layer \textit{"owns"} the weights $w$ it imposes on the incoming input vector $x$.
	
	\item \textit{Input layer:} \\A pseudo-layer that supplies upto $m$ input examples in a single batch, $m \geq 1$. Each input is assumed to be an $(F+1)$-dimensional vector of real numbers: $n$ features from the dataset, and a +1 bias term. \\This layer has no weights associated with it.
	
	\item \textit{net:} the dot product of an input vector $x$ and a weight vector $w$ for a particular neuron. It is a scalar value, i.e. a real number.
	\begin{equation*}
		net = \sum\limits_{i=0}^{(N+1)}{w_i\cdot{x_i}}
	\end{equation*}

	\item \textit{Activation function:} a function $F$ which maps input values from one space to another, thus allowing a trained network to convolute the input into arbitrarily complex functions. It acts on the scalar value of \textit{net} calculated by the neuron, and calculates another scalar $z$, which is output by the neuron.\\
	For example, if we use the \textit{sigmoid activation function}: 
	\begin{equation*}
	z = F(net) = \sigma(net) = \frac{1}{1+e^{-net}}
	\end{equation*}
	
	In general, $F$ is a function of the weights and the input, i.e. 
	\begin{equation*}
	z = F(w,x) = F(w_1,w_2,...,w_{(N+1), },  x_1,x_2,...,x_{(N+1)})
	\end{equation*}
	
	\item \textit{Gradient (general usage):} The (partial) derivative of a variable with respect to another variable. \\
	E.g. gradient of $z = F(w,x)$ with respect $w_i \Rightarrow (\frac{\partial z}{\partial w_i})$, or $x_i \Rightarrow (\frac{\partial z}{\partial x_i})$.  %Source: maths.tcd.ie/~dwilkins/LaTeXPrimer/Calculus.html
	
	\item \textit{Gradient vector}: The vector of all such Gradients (usually for the inputs or weights of a particular neuron). \\
	E.g. 
	\begin{equation*}
	\delta  = \begin{bmatrix}	   % Source: tex.stackexchange.com/q/335957/110560
		\frac{\partial z}{\partial w_1}  &  \frac{\partial z}{\partial w_2}  &  \hdots  &  \frac{\partial z}{\partial w_i}  &  \hdots  &  \frac{\partial z}{\partial w_{(N+1)}}
	\end{bmatrix} 
	\end{equation*}
	

\end{description}	

\subsection{When considering a single neuron:}
\begin{description}[font=$\bullet$ \scshape\bfseries]
	\item \textit{Inputs:}  the inputs to a neuron form a vector
	$ x = \begin{bmatrix} x_1, x_2, x_3,...,x_{(N+1)} \end{bmatrix} $. 
	We index a particular input as $x_i$.
	
	\item \textit{Weights:}  the weights on a neuron form a vector
	$ w = \begin{bmatrix} w_1, w_2, w_3,...,w_{(N+1)} \end{bmatrix} $. 
	We index a particular input as $w_i$.
	
	\item \textit{Outputs:}  The output from a neuron is the scalar value $z$, calculated as the activated dot product of the input and weight vectors, i.e. 
	\begin{equation*}
		z = F(net) = F(\sum\limits_{i=0}^{(N+1)}{w_i\cdot{x_i}})
	\end{equation*}
	 
		
	
\end{description}	


\subsection{When considering neurons in multiple layers:}

\begin{description}[font=$\bullet$ \scshape\bfseries, itemsep=6pt]	% Source: tex.stackexchange.com/a/74281/110560	
	
	\item Let $l_c$ represent the layer currently under consideration: 
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=-2pt] % Source: tex.stackexchange.com/a/86055/110560 and tex.stackexchange.com/a/6086/110560 
		\item When we talk about the input pseudo-layer, we use $l_{inp}$.
		\item When we talk about the output layer, we use $l_{out}$ or $l_{o}$ or $l_L$.
		\item When we talk about a layer, and it's previous and next layers, we use $l_{c}$, $l_{c-1}$ and $l_{c+1}$. 
		\item The first hidden layer is $l_1$, which takes inputs from the input pseudo-layer $l_i$. 
		\item The final hidden layer is the output layer, $l_o$. 
		\item The hidden layer \textbf{before} the output layer (called the \textit{penultimate layer}), is $l_p$.
	We thus have $c \in \{inp, 1,2,3 \hdots L-1, o\}$
	\end{description}
	
	\item The $i^{th}$ node in layer $l_c$ is $n^{l_c}_{i}$.
	
	\item The current layer $l_c$ is assumed to have $|l_c| = (N_c + 1)$ units: $(N_c)$ neurons and one bias unit. The output layer is the exception to this: it only has $K$ \textit{output neurons} and no bias unit, thus $|l_o| = N_o = K$. \\
	The pseudo-input layer has 
	Consequently, each neuron in $l_c$ takes $(N_{c-1} + 1)$ inputs from layer $l_{c-1}$. There are weights on all these connections. \\ 
	We may set the current layer's bias unit to any arbitrary non-zero value...for convenience we assume it is +1. 
		
		
	\item \textit{Initial Inputs:} 
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=-2pt]
	\item The inputs to the network as a whole are assumed to be \textit{"outputs"} from the input pseudo-layer. These inputs are thus denoted by: ${}_{1}{z}^{l_{inp}}_{i}, {}_{2}{z}^{l_{inp}}_{i}, \hdots, {}_{|l_{inp}|}{z}^{l_{inp}}_{i}$. 
	
	This is assumed to be a single example fed into the network from our dataset. $F = |l_c|-1 = N_{inp}$ is the number of features for each input example in our dataset. Our dataset is said to be \textit{F-dimensional}.
	
	E.g. if we are predicting which \textit{city} an apartment is in, from a dataset with the features \{\textit{monthly rent}, \textit{number of bedrooms}, \textit{number of bathrooms}, \textit{presence of internet connection}\}, we will have $N_{inp} = 4$ features. $|l_{inp}|$ will be 5, since we must add 
		
	\end{description}
	
	
	
		
	\item \textit{Inputs:} \\
	The input to the $i^{th}$ neuron of the current layer $l_{c}$ from the $j^{th}$ neuron of the previous layer $l_{c-1}$, is:
	\begin{equation*} {}_{j}{x}^{l_{c}}_{i} \end{equation*}
	
	 
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=-2pt]
		
	\item We can arrange these inputs into a vector:
	\begin{equation*} {x}^{l_{c}}_{i} = \begin{bmatrix} 
	{}_{1}{x}^{l_{c}}_{i} & {}_{2}{x}^{l_{c}}_{i} & \hdots & {}_{j}{x}^{l_{c}}_{i} & \hdots & {}_{|l_c|}{x}^{l_{c}}_{i}
	\end{bmatrix} \end{equation*}
	
	
	
	\end{description}
	
	Examples: 
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=0pt]
		\item ${}_{5}{x}^{l_{4}}_{7}$ is the input to the $7^{th}$ neuron of layer 4, coming from the $5^{th}$ neuron of layer 3.
		\item ${}_{6}{x}^{l_{o}}_{2}$ is the input to the $2^{nd}$ neuron of the output layer, coming from the $6^{th}$ neuron of penultimate layer.
		\item ${}_{(N_{inp}+1)}{x}^{l_{1}}_{3}$ is the input to the $3^{rd}$ neuron of the first hidden layer, coming from the $(N_{inp}+1)^{th}$ neuron - i.e. \textbf{bias neuron} - of the pseudo-input layer.
	\end{description}
  
  
  
  
  
	\item \textit{Weights:} \\
	The weight on the connection from the $j^{th}$ neuron of the previous layer $l_{c-1}$ to the $i^{th}$ neuron of the current layer $l_{c}$, is: 
	 \begin{equation*} {}_{j}{w}^{l_{c}}_{i} \end{equation*}
	 
	 \begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=2pt] 
	 	\item This is value is used to weight the input ${}_{j}{x}^{l_{c}}_{i}$ which is incident on our neuron. 
	 	
	 	\item We can arrange these weights for each neuron into a vector column: 
		 \begin{equation*} {w}^{l_{c}}_{i} = 
			 \begin{bmatrix} 
				 {}_{1}{w}^{l_{c}}_{i} \\ {}_{2}{w}^{l_{c}}_{i} \\ \hdots \\ {}_{j}{w}^{l_{c}}_{i} \\ \hdots \\ {}_{(N_{c-1}+1)}{w}^{l_{c}}_{i}
			 \end{bmatrix}
		 \end{equation*} 
		 We say that this weight vector is \textit{"owned"} by the $i^{th}$ neuron in layer $l_c$.
	 
		\item Let the weights of an entire layer $l_c$ be $w^{l_c}$; we can represent this as a matrix, where every neuron's weigh vector lies along one column. \\
		\begin{equation*}
		w^{l_c} = \begin{bmatrix} 
		{}_{1}{w}^{l_{c}}_{1} 	& {}_{1}{w}^{l_{c}}_{2} & \hdots & {}_{1}{w}^{l_{c}}_{i} 	& \hdots 				&  {}_{1}{w}^{l_{c}}_{N_c}\\ 
		
		{}_{2}{w}^{l_{c}}_{1} 	& {}_{2}{w}^{l_{c}}_{2} & \hdots & {}_{2}{w}^{l_{c}}_{i} 	& \hdots 				&  {}_{2}{w}^{l_{c}}_{N_c}\\
		
		\hdots & \hdots & \hdots & \hdots & \hdots & \hdots\\ 
		
		\hdots & \hdots & {}_{j}{w}^{l_{c}}_{i-1}	& {}_{j}{w}^{l_{c}}_{i} & \hdots & \hdots \\ 
		
		\hdots & \hdots & \hdots & \hdots & \hdots & \hdots\\ 
		
		\hspace{0.1em} {}_{(N_{c-1}+1)}{w}^{l_{c}}_{1} & \hdots & {}_{(N_{c-1}+1)}{w}^{l_{c}}_{i} & \hdots & \hdots & {}_{(N_{c-1}+1)}{w}^{l_{c}}_{N_c}
		\end{bmatrix}
		\end{equation*}
	
		where:
		\begin{description}[font=$\Rightarrow$ \scshape\bfseries, topsep=-2pt, itemsep=-2pt]
			\item $w^{l_c}$ has dimensions: $(N_{c-1} + 1)\times(N_{c})$
			\item ${}_{j}{w}^{l_{c}}_{i}$ denotes the $j^{th}$ row and $i^{th}$ column (just like normal matrix indexing).
			\item Each column is a neuron in the current layer $l_c$. 
			\item $c \in \{1 ,2, \hdots, L-1, out\}$. The input layer is a pseudo-layer and has no associated weight matrix. 
			\item $(c-1) \in \{inp, 1, 2, \hdots, L-1\}$
			\item $N_c$ is the number of neurons in the current layer.
			\item $N_{c-1}$ is the number of neurons in the previous layer.
			\item $(N_{c-1} + 1)$ is the number of inputs to each neuron in the current layer (+1 because of bias unit from previous layer).
		\end{description}
		
		 
		\item The network can thus be represented as an array of weight matrices: 
		\begin{equation*}
			w = \begin{bmatrix} w^{l_{1}} & w^{l_{2}} & \hdots & w^{l_{L-1}} & w^{l_{o}} \end{bmatrix}
		\end{equation*}
		However, while it may initially seem that these matrices are multipliable, looking at the formula for the dimensionality of a matrix in layer $l_c$, the matrices in the array have following dimensions:
		\begin{equation*}
			w_{dims} = 
			\begin{bmatrix} 
				(N_{inp} + 1)\times(N_{1}) & (N_{1} + 1)\times(N_{2}) & (N_{2} + 1)\times(N_{3}) \\ 
				\hdots & (N_{c-1} + 1)\times(N_{c}) & (N_{c} + 1)\times(N_{c+1}) &\\ 
				\hdots & (N_{L-2} + 1)\times(N_{L-1}) & (N_{L-1} + 1)\times(N_{o} = K) 
			\end{bmatrix}
		\end{equation*}
		
		We can thus see that each adjacent pair in not directly multipliable. In the forward pass, given as input a $(m)\times(N_{inp})$ dataset, we must first add a bias column to this dataset, then multiply with the first matrix, giving us temporary outputs with dimensions $(m)\times(N_{1})$. We must then add a bias column again and then multiply with the next matrix, and so on until we get our final predictions, which is a binary matrix of $(m)\times(K)$. 
		
		Adding a bias column to the weight matrix at each step is equivalent to adding a bias unit, since columns are neurons. 
		
	 
	\end{description}
  
  
  
  
	
	\item \textit{Outputs:} \\
	The output from the $i^{th}$ neuron of the current layer $l_{c}$ to the $j^{th}$ neuron of the next layer, $l_{(c+1)}$ is: 
	\begin{equation*} {}^{l_{c}}_{i}{z}_{j} \end{equation*}
	
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=-2pt]
		\item For the output layer $l_o$ we use the notation $o_j$ to denote the output predicted by the $j^{th}$ neuron in the output layer. This is the value the network predicts for the $j^{th}$ class. 
	
	\item Since a neuron sends only one value to all the neurons in the next layer, we have: ${}^{l_{c}}_{i}{z}_{1} = {}^{l_{c}}_{i}{z}_{2} = \hdots = {}^{l_{c}}_{i}{z}_{N_{c+1}} = {}^{l_{c}}_{i}{z}$ ($\leftarrow$ for convenience).
	
	\item The $i^{th}$ neuron in layer $l_c$ calculates this output value as:
	\begin{equation*} {}^{l_{c}}_{i}{z} = F(\sum\limits_{i=0}^{(N+1)} {w_i\cdot{x_i}}) \end{equation*}
	
	\end{description}
	 
	Examples: 
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=-2pt]
		\item ${}^{l_{4}}_{1}{z}_{19}$ is the output from the $1^{st}$ neuron of layer 4, which is sent to the $19^{th}$ neuron of layer 5.
		\item $o_{17}$ (or, ${}^{l_{o}}_{17}{z}$) is the value predicted for the $17^{th}$ output class, from the $17^{th}$ \textit{output neuron}. \\
		If we are using our neural network for character recognition then $K=26$, and $o_{17}$ would tell us if (or how strongly) our network thinks an input example is the alphabet $Q$.
	\end{description}
		
	
	\item Obviously we have ${}^{l_{c}}_{i}{z}_{j}$ = ${}_{i}{x}^{l_{c+1}}_{j}$, i.e. the output from the current layer's $i^{th}$ neuron to the next layer's $j^{th}$ neuron, is the input to the next layer's $j^{th}$ neuron from the current layer's $i^{th}$ neuron.
	
	
	
	
	\item When we are juggling calculations involving several neurons from adjacent layers, \textbf{we use these notations to clarify which neuron is being considered at the moment, and whether its inputs, outputs or weights are being considered.} \\
	The trick to using the notation is to remember that: 
	\begin{description}[font=$-$ \scshape\bfseries, topsep=-2pt, itemsep=-2pt]
		\item The \textbf{superscript $l_c$ always denotes the current layer, and it's corresponding subscript is the neuron we are considering}.
		\item The left subscript always denotes the layer on the left, and the right subscript always denotes the layer on the right (assuming the network \textit{"flows"} from left to right in a forward pass)
	\end{description}
	
	

	
	
\end{description}